<?php

use Core\Database;
use Core\Router;

$config = include 'config.php';

$db = (new Database($config))->connection();


$uri = trim($_SERVER['REQUEST_URI'], '/');

require Router::load('app/routes.php')->direct($uri);