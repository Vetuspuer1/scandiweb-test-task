<?php

namespace App;

class Furniture extends Product
{
    protected $model = 'Furniture';
    protected $attribute = 'Dimensions';
    protected $unit = 'CM';

    public function __construct($sku, $name, $price, $height, $width, $length)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->attribute_value = strval($height) . 
        "x" . strval($width) . "x" . strval($length);
    }

}