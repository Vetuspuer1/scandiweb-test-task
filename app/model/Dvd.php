<?php

namespace App;

class Dvd extends Product
{
    protected $model = 'Dvd';
    protected $attribute = 'Size';
    protected $unit = 'MB';

    public function __construct($sku, $name, $price, $size)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->attribute_value = $size;
    }
    
}