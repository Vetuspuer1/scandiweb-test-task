<?php 

namespace App;

abstract class Product
{
    protected $sku;
    protected $name;
    protected $price;
    protected $attribute_value;
    protected static $tableName = 'products';


    public static function all() 
    {
        global $db;

        $sql = 'SELECT * FROM ' . static::$tableName;

        return $db->query($sql)->fetchAll();
    }

    public static function delete($products) 
    {
        global $db;
        $limit = 100;
        $productCount = count($products);

        if(count($products) <= $limit) {
           
            $sql = 'DELETE FROM ' . static::$tableName . ' WHERE SKU IN';
            $sql .= ' ("' . join('","', $products) .'");';

            $stmt = $db->prepare($sql);
            $stmt->execute();

        } else {

            for ($i = 0; $i < ceil($productCount / $limit); $i++) {
                
                $sql = 'DELETE FROM ' . static::$tableName . ' WHERE SKU IN ("';
                
                for ($j = 0; $j < $limit; $j++) {
                   
                    if ($j + 1 == $limit || count($products) == 1) {
                        $sql .= $products[0] . '")';
                        array_shift($products);
                        break;
                    } else {
                        $sql .= $products[0] . '", "';
                    }
                    array_shift($products);     
                }

                $sql .= ' LIMIT ' . $limit . ';';

                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
        }
           
    }

    public function insert() 
    {
        global $db;

        $checkSku = $this->checkSku();

        if (!$checkSku['status']) {

            return $checkSku;

        } else {

            $array = get_object_vars($this);

            $arrayKeys = array_keys($array);
            $arrayValues = array_values($array);
            $arrayValuePlaceHolders = [];

            for ($i = 0; $i < count($arrayKeys); $i++) {
                $arrayValuePlaceHolders[] = '?';
            }

            $sql = 'INSERT INTO ' . static::$tableName;

            $sql.= ' (`' . join('`,`', $arrayKeys) .'`)';
            $sql.= ' VALUES ';
            $sql.= '(' . join(',', $arrayValuePlaceHolders) .')';
            
            try {
                $db->prepare($sql)->execute($arrayValues);
            } catch (PDOException $e) {
                return [
                    'status' => false,
                    'message' => 'Problem creating post'
                ];
            }
        }

        return [
            'status' => true,
            'message' => 'Success!'
        ];
    }

    private function checkSku() 
    {
        global $db;

        $sku = $this->sku;
        $sql = 'SELECT sku FROM products WHERE sku LIKE "' . $sku . '";';
        
        try {
            $result = $db->query($sql)->fetchAll();
        } catch (PDOException $e) {
            return [
                'status' => false,
                'message' => $e->getMessage()
            ];
        }

        if (count($result) == 0) {
            return [
                'status' => true,
                'message' => 'Success!'
            ];
        } else {
            return [
                'status' => false,
                'message' => 'Error, SKU already exists!'
            ];
        }
       
    }
}