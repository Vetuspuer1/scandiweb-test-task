<?php

namespace App;

class Book extends Product
{
    protected $model = 'Book';
    protected $attribute = 'Weight';
    protected $unit = 'KG';

    public function __construct($sku, $name, $price, $weight)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->attribute_value = $weight;
    }

}