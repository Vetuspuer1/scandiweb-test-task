<?php 

use App\Product;

$products = Product::all();

$action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
$checkbox = filter_input(INPUT_POST, 'checkbox', FILTER_SANITIZE_STRING);

if (isset($checkbox) && isset($action) && $action === 'delete') {
    $checkbox = $_POST['checkbox'];
    Product::delete($checkbox);
    header('Location: /');
}

?>

<?php include 'partials/header.php' ?>

<form method="post">
    <div class="header">
        <h1>Product List</h1>
        <div class="buttons">
            <a class="button" href="/add">Add</a>
            <button class="button cancel" type="submit" name="action" value="delete">
                Mass Delete
            </button>
        </div>
    </div>
    <div class="products">
    <?php
    foreach ($products as $row) {
        echo '<div class="item">
			<input class="checkbox" type="checkbox" name="checkbox[]" value="'.$row->sku.'">
			<ul>
				<li>' . $row->sku . '</li>
				<li>' . $row->name . '</li>
				<li>' . number_format($row->price, 2, '.', '') . ' $</li>
				<li>' . $row->attribute . ': ' . $row->attribute_value . ' '.$row->unit.'</li>
			</ul>
		</div>';
    }
    ?>
    </div>
</form>

<?php include 'partials/footer.php' ?>