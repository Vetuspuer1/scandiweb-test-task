<?php 

use App\Book;
use App\Dvd;
use App\Furniture;

$action = filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING);
$sku = filter_input(INPUT_POST, 'sku', FILTER_SANITIZE_STRING);
$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
$price = filter_input(INPUT_POST, 'price', FILTER_VALIDATE_FLOAT);
$weight = filter_input(INPUT_POST, 'weight', FILTER_SANITIZE_STRING);
$size = filter_input(INPUT_POST, 'size', FILTER_SANITIZE_STRING);
$height = filter_input(INPUT_POST, 'height', FILTER_VALIDATE_INT);
$width = filter_input(INPUT_POST, 'width', FILTER_VALIDATE_INT);
$length = filter_input(INPUT_POST, 'length', FILTER_VALIDATE_INT);

$errors = "";

if (isset($action)) {
    switch ($action) {
    case 'book':
        $book = new Book($sku, $name, $price, $weight);
        $errors = $book->insert();
        break;
    case 'dvd':
        $dvd = new Dvd($sku, $name, $price, $size);
        $errors = $dvd->insert();
        break;
    case 'furniture':
        $furniture = new Furniture($sku, $name, $price, $height, $width, $length);
        $errors = $furniture->insert();
        break;
    }

    if (empty($errors) || $errors['status']) {
        header('Location: /');
    } else {
        echo "<div class='bigerror'>" . $errors['message'] . "</div>";
    }
}

?>

<?php include 'partials/header.php' ?>

<form method="post" id="form">
    <div class="header">
        <h1>Product Add</h1>
        <div class="buttons">
            <span id="save">
                <button class="button" type="submit" name="action" value="book">
                    Save
                </button>
            </span>
            <a class="button cancel" href="../">Cancel</a>
        </div>
    </div>
    <div class="input-forms">
        <div class="input-element">
            <label for="SKU">SKU</label>
            <div>
                <input type="text" id="sku" name="sku">
                <span class="error" id="skuerror"></span>
            </div>
        </div>
        <div class="input-element">
            <label for="name">Name</label>
            <div>
                <input type="text" id="name" name="name">
                <span class="error" id="nameerror"></span>
            </div>
        </div>
        <div class="input-element">
            <label for="price">Price ($)</label>
            <div>
                <input type="text" id="price" name="price">
                <span  class="error" id="priceerror"></span>
            </div>
        </div>
        <div class="input-element">
            <label for="type-switcher">Type Switcher</label>
            <select id="type-switcher" name="type-switcher">
                <option id="book" value="book">Book</option>
                <option id="dvd" value="dvd">DVD</option>
                <option id="furniture" value="furniture">Furniture</option>
            </select>
        </div>
        <div id="dynamic_field">
            <div class="input-element">
                <p>Please, provide weight</p>
                <label for="weight">Weight (KG)</label>
                <div>
                    <input type="text" id="weight" name="weight">
                    <span class="error" id="weighterror"></span>
                </div>
            </div>
        </div>
    </div>
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js">
</script>
<script src="../public/scripts/app.js"></script>

<?php include 'partials/footer.php' ?>