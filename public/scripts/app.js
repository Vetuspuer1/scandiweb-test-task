$(document).ready(function(){
    $("#type-switcher").change(function(){
        var option = $("#type-switcher option:selected").text();
        switch (option) { 
            case 'DVD': 
                $('#dynamic_field').html("\
                    <div class='input-element'>\
                        <p>Please, provide size</p>\
                        <label for='size'>Size (MB)</label>\
                        <div>\
                            <input type='text' id='size' name='size'>\
                            <span class='error' id='sizeerror'></span>\
                        </div>\
                    </div>\
                ");
                $('#save').html('\
                    <button class="button" type="submit" name="action" value="dvd">\
                        Save\
                    </button>\
                ');
                break;
            case 'Furniture': 
                $('#dynamic_field').html("\
                    <div class='input-element'>\
                        <p>Please, provide dimensions</p>\
                        <label for='height'>Height (CM)</label>\
                        <div>\
                            <input type='text' id='height' name='height'>\
                            <span class='error' id='heighterror'></span>\
                        </div>\
                    </div>\
                    <div class='input-element'>\
                        <label for='weight'>Width (CM)</label>\
                        <div>\
                            <input type='text' id='width' name='width'>\
                            <span class='error' id='widtherror'></span>\
                        </div>\
                    </div>\
                    <div class='input-element'>\
                        <label for='length'>Length (CM)</label>\
                        <div>\
                            <input type='text' id='length' name='length'>\
                            <span class='error' id='lengtherror'></span>\
                        </div>\
                    </div>\
                ");
                $('#save').html('\
                    <button class="button" type="submit" name="action" value="furniture">\
                        Save\
                    </button>\
                ');
                break;
            default:
                $('#dynamic_field').html("\
                    <div class='input-element'>\
                        <p>Please, provide weight</p>\
                        <label for='weight'>Weight (KG)</label>\
                        <div>\
                            <input type='text' id='weight' name='weight'>\
                            <span class='error' id='weighterror'></span>\
                        </div>\
                    </div>\
                ");
                $('#save').html('\
                    <button class="button" type="submit" name="action" value="book">\
                        Save\
                    </button>\
                ');
                break;
        }
    });
});

$('#form').submit(function() {
    let result = true;
    let empty = "Please, submit required data";
    let wrongData = "Please, provide the data of indicated type"
    
    if ($("#sku").val().length == 0) {
        $('#skuerror').text(empty);
        result = false;
    } else {
        $('#skuerror').text("");
    }

    if ($("#name").val().length == 0) {
        $('#nameerror').text(empty);
        result = false;
    } else {
        $('#nameerror').text("");
    }

    if ($("#price").val().length == 0) {
        $('#priceerror').text(empty);
        result = false;
    } else if (!$.isNumeric($("#price").val())) {
        $('#priceerror').text(wrongData);
        result = false;
    } else {
        $('#priceerror').text("");
    }

    if ($('#weight').length) {
        if ($("#weight").val().length == 0) {
            $('#weighterror').text(empty);
            result = false;
        } else if (!$.isNumeric($("#weight").val())) {
            $('#weighterror').text(wrongData);
            result = false;
        } else {
            $('#weighterror').text("");
        }
    }
    
    if ($('#size').length) {
        if ($("#size").val().length == 0) {
            $('#sizeerror').text(empty);
            result = false;
        } else if (!$.isNumeric($("#size").val())) {
            $('#sizeerror').text(wrongData);
            result = false;
        } else {
            $('#sizeerror').text("");
        }
    }

    if ($('#height').length) {
        if ($("#height").val().length == 0) {
            $('#heighterror').text(empty);
            result = false;
        } else if (!$.isNumeric($("#height").val())) {
            $('#heighterror').text(wrongData);
            result = false;
        } else {
            $('#heighterror').text("");
        }
    }
    
    if ($('#width').length) {
        if ($("#width").val().length == 0) {
            $('#widtherror').text(empty);
            result = false;
        } else if (!$.isNumeric($("#width").val())) {
            $('#widtherror').text(wrongData);
            result = false;
        } else {
            $('#widtherror').text("");
        }
    }
    
    if ($('#length').length) {
        if ($("#length").val().length == 0) {
            $('#lengtherror').text(empty);
            result = false;
        } else if (!$.isNumeric($("#length").val())) {
            $('#lengtherror').text(wrongData);
            result = false;
        } else {
            $('#lengtherror').text("");
        }
    }

    return result;
});